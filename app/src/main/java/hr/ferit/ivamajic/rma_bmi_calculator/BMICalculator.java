package hr.ferit.ivamajic.rma_bmi_calculator;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BMICalculator extends AppCompatActivity implements View.OnClickListener {

    private static final String POTHRANJEN = "Pothranjen";
    private static final String ZDRAV = "Zdrav";
    private static final String DEBEO = "Debeo";
    private static final String PRETIO = "Pretio";

    private static final String POTHRANJEND = "Opis pothranjenog";
    private static final String ZDRAVD = "Opis zdravog";
    private static final String DEBEOD = "Opis debelog";
    private static final String PRETIOD= "Opis pretilog";

    TextView masa, visina;
    TextView textResult, numResult, textDescription;
    Button bCalculate;
    ImageView imageResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmicalculator);
        initializeUI();
    }

    private void initializeUI() {
        masa = (TextView) findViewById(R.id.masa);
        visina = (TextView) findViewById(R.id.visina);
        numResult = (TextView) findViewById(R.id.numResult);
        textResult = (TextView) findViewById(R.id.textResult);
        imageResult=(ImageView) findViewById(R.id.imageResult);
        textDescription=(TextView) findViewById(R.id.textDescription);
        this.bCalculate = (Button) findViewById(R.id.bCalculate);
        this.bCalculate.setOnClickListener(this);


    }
    public void onClick(View view) {

        float masaV=Float.valueOf(masa.getText().toString());
        float visinaV=Float.valueOf(visina.getText().toString());
        if (masaV<=350 && visinaV<=2.5) {
            float rezultatV = masaV / (visinaV * visinaV);
            numResult.setText(Float.toString(rezultatV));
            if (rezultatV < 18.5) {
                textResult.setText(POTHRANJEN);
                textDescription.setText(POTHRANJEND);
                //   imageResult.setImageResource(R.drawable.pothranjen);
            } else if (rezultatV >= 18.5 && rezultatV <= 24.9) {
                textResult.setText(ZDRAV);
                textDescription.setText(ZDRAVD);
                //  imageResult.setImageResource(R.drawable.pothranjen);
            } else if (rezultatV >= 25 && rezultatV <= 29.9) {
                textResult.setText(DEBEO);
                textDescription.setText(DEBEOD);
                //  imageResult.setImageResource(R.drawable.pothranjen);
            } else if (rezultatV >= 30) {
                textResult.setText(PRETIO);
                textDescription.setText(PRETIOD);
                //   imageResult.setImageResource(R.drawable.pothranjen);
            }
        }

    }
}
